QCrop version history
=====================


## 0.0.1

### Added
-   README
-   Properly parsing args
### Fixed
-   Crash when running with no parameter
