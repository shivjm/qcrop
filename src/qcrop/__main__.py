import sys
from pathlib import Path
from loguru import logger
import argparse

from PyQt5.QtWidgets import QApplication, QDialog
from PyQt5.QtGui import QPixmap

from .ui import QCrop
from . import __version__


def main():
    parser = argparse.ArgumentParser(prog="qcrop")
    parser.add_argument(
        "--version", action="version", version="%(prog)s {}".format(__version__)
    )
    parser.add_argument("image", help="Path to the image file to crop.", type=Path)
    parser.add_argument(
        "output", help="Path to save new file at.", nargs="?", type=Path
    )
    args = parser.parse_args()

    input_path = args.image
    output_path = args.output

    if output_path is None:
        output_path = input_path.with_suffix(".cropped" + input_path.suffix)

    if not input_path.is_file():
        raise ValueError(f"Not a valid file: {input_path}")

    if input_path.is_file():
        APP = QApplication(sys.argv)
        img = QPixmap(str(input_path))
        WIN = QCrop(img)

        rc = WIN.exec()
        if rc == QDialog.Accepted:
            WIN.image.save(str(output_path))

        APP.exit()


if __name__ == "__main__":
    main()
