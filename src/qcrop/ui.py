import math

from PyQt5.QtWidgets import QDialog
from PyQt5.QtCore import QRect, QSize, QSignalBlocker, pyqtSlot, QEvent, Qt
from loguru import logger

import qcrop

from .qcrop_ui import Ui_QCrop

MARGIN_H = 48
MARGIN_V = 120


class QCrop(QDialog):
    def __init__(self, pixmap, parent=None):
        super().__init__(parent)

        self._ui = Ui_QCrop()
        self._ui.setupUi(self)

        self.setWindowTitle("QCrop - v{}".format(qcrop.__version__))

        self.image = pixmap
        self._original_image = pixmap
        self._original = QRect(0, 0, self.image.width(), self.image.height())

        self._scaled = self._original_image.scaled(
            self.width(),
            self.height(),
            Qt.KeepAspectRatio,
        )

        self._ui.selector.crop = QRect(0, 0, self.image.width(), self.image.height())
        self._ui.selector.setPixmap(self.image)

        self._ui.workspace.installEventFilter(self)

        self._ui.spinBoxX.setMaximum(self._original.width() - 1)
        self._ui.spinBoxY.setMaximum(self._original.height() - 1)
        self._ui.spinBoxW.setMaximum(self._original.width())
        self._ui.spinBoxH.setMaximum(self._original.height())
        self.update_crop_values()

        self.resize(
            self._original.width() + MARGIN_H, self._original.height() + MARGIN_V
        )

    def update_crop_area(self):
        values = self.crop_values()
        if self._ui.selector.crop != values:
            self._ui.selector.crop = values
            self._ui.selector.update()

    def crop_values(self):
        return QRect(
            self._ui.spinBoxX.value(),
            self._ui.spinBoxY.value(),
            self._ui.spinBoxW.value(),
            self._ui.spinBoxH.value(),
        )

    def update_crop_values(self):
        crop = self._ui.selector.crop

        scale = self._original_image.width() / self._scaled.width()

        self._ui.spinBoxX.blockSignals(True)
        self._ui.spinBoxX.setValue(math.floor(crop.x() * scale))
        self._ui.spinBoxX.blockSignals(False)
        self._ui.spinBoxY.blockSignals(True)
        self._ui.spinBoxY.setValue(math.floor(crop.y() * scale))
        self._ui.spinBoxY.blockSignals(False)
        self._ui.spinBoxW.blockSignals(True)
        self._ui.spinBoxW.setValue(math.floor(crop.width() * scale))
        self._ui.spinBoxW.blockSignals(False)
        self._ui.spinBoxH.blockSignals(True)
        self._ui.spinBoxH.setValue(math.floor(crop.height() * scale))
        self._ui.spinBoxH.blockSignals(False)

    def eventFilter(self, widget, event):
        if event.type() == QEvent.Resize and widget is self._ui.workspace:
            selector = self._ui.selector
            (scaled,) = (
                self._original_image.scaled(
                    widget.width(),
                    widget.height(),
                    Qt.KeepAspectRatio,
                ),
            )
            logger.debug(
                "Scaling pixmap to {}x{}: {}x{} to {}x{} (selector: {}x{})",
                widget.width(),
                widget.height(),
                self._original_image.width(),
                self._original_image.height(),
                scaled.width(),
                scaled.height(),
                selector.width(),
                selector.height(),
            )
            self._scaled = scaled
            selector.setPixmap(
                self._scaled,
            )
            selector.setMinimumSize(QSize(scaled.width(), scaled.height()))
            return True
        return super(QCrop, self).eventFilter(widget, event)

    @pyqtSlot()
    def reset_crop_values(self):
        self._ui.spinBoxX.setValue(0)
        self._ui.spinBoxY.setValue(0)
        self._ui.spinBoxW.setValue(self._scaled.width())
        self._ui.spinBoxH.setValue(self._scaled.height())

    @pyqtSlot()
    def accept(self):
        if self._ui.selector.crop != self._original:
            logger.info(
                "Cropping {}x{} using {}",
                self.image.width(),
                self.image.height(),
                self._ui.selector.crop,
            )
            self.image = self.image.copy(self._ui.selector.crop)
            super().accept()
        else:
            super().reject()
