# QCrop (fork)

A basic PyQt image cropping tool. This fork implements:

* Automatically scaling the image to the available area
* Specifying an output filename
* Better logging

## Install

As a library:

```bash
pip install qcrop
```

As an application with [pipx](https://pypa.github.io/pipx/):

```bash
pipx install qcrop
```

## Usage

### As an application

```bash
qcrop <path/to/image.ext> [path/to/output.ext]
```

For example, <kbd>qcrop some-image.png new-image.png</kbd>.

If the output path is omitted, QCrop will default to adding `.cropped` before the extension of the input path.

The supported image formats are [those handled by the Qt framework][formats]:

* BMP
* GIF
* JPG
* PNG
* PBM
* PGM
* PPM
* XBM
* XPM

### As a Python module

Sample code:

```python
from PyQt5.QtGui import QPixmap
from qcrop.ui import QCrop

original_image = QPixmap(...)
crop_tool = QCrop(original_image)
status = crop_tool.exec()

if status == Qt.Accepted:
    cropped_image = crop_tool.image
# else crop_tool.image == original_image
```

### User interface

Use the mouse to select the image area to crop. You can also use the <samp>X</samp>, <samp>Y</samp>, <samp>Width</samp> and <samp>Height</samp> fields to precisely adjust the area by typing, using the spinners, or using the mouse wheel. Note that the displayed image is scaled to the size of the dialog, but the controls show the correct numbers for the original image.

Use <samp>Reset</samp> to set the crop area to the full image.

Once done, use <samp>Ok</samp> to accept the crop or <samp>Cancel</samp> to exit.

![](screenshot.png)


   [formats]: https://doc.qt.io/qt-5/qpixmap.html#reading-and-writing-image-files
